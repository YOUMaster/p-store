import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from '../Shared/api.service';

export interface IProduct {
  id: number;
  title: string;
  image: string;
  price: number;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {
  public products: IProduct[];
  private _subscriptions: Subscription[] = [];
  constructor(private _apiSevice : ApiService) { }
  
  public ngOnInit(): void {
    this._subscriptions = [
      this._apiSevice.getProducts().subscribe((products: IProduct[]) => {
        this.products = products;
      })
    ];
  }

  public ngOnDestroy(): void {
    if (this._subscriptions) {
      for (let subscription of this._subscriptions) {
        subscription.unsubscribe();
      }
      this._subscriptions = [];
    }
  }
}
