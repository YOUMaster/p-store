import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';


const routes: Routes = [
  { path: 'products', component: ProductsComponent },
  { path: 'product', redirectTo: 'products', pathMatch: 'full' },
  { path: 'product/:id', component: ProductComponent },
  { path: '', redirectTo: 'products', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
