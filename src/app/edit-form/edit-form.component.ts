import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IProduct } from '../products/products.component';

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent implements OnInit {
  @Input() public product: IProduct;

  public amount: number;
  public isSubmited: boolean = false;
  public form: FormGroup;

  constructor() { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(this.product.title, Validators.required),
      price: new FormControl(this.product.price, [Validators.required, Validators.pattern(/^\d+$/)])
    });
  }

  public onInput() {
    this.isSubmited = false;
  }

  public onSubmit() {
    this.product.title = this.form.value.title;
    this.product.price = this.form.value.price;
    console.log("this.product", this.product);
    this.isSubmited = true;
  }
}
