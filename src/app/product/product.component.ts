import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { ApiService } from '../Shared/api.service';
import { IProduct } from '../products/products.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {
  public id: number;
  public subscription: Subscription;
  public product: IProduct;

  constructor(
    private _route: ActivatedRoute,
    private _apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.id = +this._route.snapshot.params['id'];

    this._route.params.subscribe((params: Params) => {
      this.id = +params['id'];
    });

    this.subscription = this._apiService.getProductDetails(this.id).subscribe((product: IProduct) => {
      this.product = product;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
