import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { IProduct } from '../products/products.component';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private _http: HttpClient
  ) { }

  public getProducts(): Observable<IProduct[]> {
    return this._http.get<IProduct[]>('http://j4456.mocklab.io/products').pipe(catchError(this._handleError<IProduct[]>('getProducts', [])));
  }

  public getProductDetails(id: number): Observable<IProduct> {
    return this._http.get<IProduct>('http://j4456.mocklab.io/product/' + id).pipe(catchError(this._handleError<IProduct>('getProducts', undefined)));
  }

  private _handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this._log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private _log(message: string): void {
    alert("Something went wrong! Please try again later.");
    throw Error(message);
  }
}
